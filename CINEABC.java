
import java.util.Random;
import java.util.Scanner;

public class CINEABC {

    public static void main(String[] args) {

        // Variáveis de controle
        Scanner sc = new Scanner(System.in);
        String disponivel = "[ ]", meia = "[M]", inteira = "[I]";
        int maxLin = 10, maxCol = 10;
        int limInfP = 0, limSupP = 9;
        char opc;
        String ingresso;
        String pol;
        boolean oi = true;
        String continuar;
        char opcao;

//------------------------------------------------------------------------------
//                             CARGA INICIAL 
//------------------------------------------------------------------------------        
        // Input 9 sessoes 10 X 10
        String[][] sessao1 = new String[maxLin][maxCol];
        String[][] sessao2 = new String[maxLin][maxCol];
        String[][] sessao3 = new String[maxLin][maxCol];
        String[][] sessao4 = new String[maxLin][maxCol];
        String[][] sessao5 = new String[maxLin][maxCol];
        String[][] sessao6 = new String[maxLin][maxCol];
        String[][] sessao7 = new String[maxLin][maxCol];
        String[][] sessao8 = new String[maxLin][maxCol];
        String[][] sessao9 = new String[maxLin][maxCol];

        // cadatro de poltronas das sessões
        cadastraPoltronas(sessao1, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao2, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao3, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao4, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao5, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao6, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao7, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao8, maxLin, maxCol, disponivel);
        cadastraPoltronas(sessao9, maxLin, maxCol, disponivel);

        // Preenchimento aleatório de 20% das poltronas por sessão
        preenchimetoAleatorio(sessao1, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao2, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao3, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao4, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao5, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao6, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao7, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao8, limInfP, limSupP, meia, inteira);
        preenchimetoAleatorio(sessao9, limInfP, limSupP, meia, inteira);
//------------------------------------------------------------------------------
//                               MENU MÓDULOS
//------------------------------------------------------------------------------        
        boolean valida = true;
        int opcaoM = 0;

        System.out.print(
                "Bem-vindo(a) ao CINE ABC\n\n");

        System.out.println(
                "MENU:\n\n"
                + "[1] Vendas de ingressos\n"
                + "[2] Histórico de ingressos\n"
                + "[3] Relatórios\n"
                + "[4] Sair\n");

        while (valida) {
            try {
                System.out.print("RESPOSTA: ");
                opcaoM = Integer.parseInt(sc.nextLine());

                while (opcaoM < 1 || opcaoM > 4) {
                    System.out.println("Entrada invalida! "
                            + "Tente novamente!");
                    System.out.print("RESPOSTA: ");
                    opcaoM = Integer.parseInt(sc.nextLine());
                }
                valida = false;

            } catch (Exception e) {
                System.out.println("Entrada invalida! "
                        + "Tente novamente!");
            }
        }

//------------------------------------------------------------------------------
//                                MENU SESSÕES
//------------------------------------------------------------------------------          
        do {
            if (opcaoM
                    == 1) {

                // cadatro das sessões
                int opcaoS = 0;

                menuSessoes();

                while (valida) {
                    try {
                        System.out.print("RESPOSTA: ");
                        opcaoS = Integer.parseInt(sc.nextLine());

                        while (opcaoS < 1 || opcaoS > 9) {
                            System.out.println("Entrada invalida! "
                                    + "Tente novamente!");
                            System.out.print("RESPOSTA: ");
                            opcaoS = Integer.parseInt(sc.nextLine());
                        }
                        valida = false;

                    } catch (Exception e) {
                        System.out.println("Entrada invalida! "
                                + "Tente novamente!");
                    }
                }
            }

            System.out.println("Digite a sessão desejada: ");
            System.out.print("> ");
            opc = (char) sc.nextInt();
            while (opc != 1 && opc != 2 && opc != 3 && opc != 5 && opc != 6
                    && opc != 4 && opc != 7 && opc != 8 && opc != 9) {
                System.out.println("opção invalida, tente novamente: ");
                opc = (char) sc.nextInt();
            }

            switch (opc) {
                case 1:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[1] SALA A | AS PANTERAS | 118 MIN | DUB | 14 ANOS\n");
                    imprimeMatriz(sessao1);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao1.length; i++) {
                            for (int j = 0; j < sessao1.length; j++) {
                                while (sessao1[i][j].equals(pol + inteira) || sessao1[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao1.length; i++) {
                            for (int j = 0; j < sessao1.length; j++) {

                                if (pol.equals(sessao1[i][j].substring(0, sessao1[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao1[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao1[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao1);

                    break;
                case 2:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[2] SALA B | MALEVOLA - DONA DO MAL | 120 MIN | DUB | 10 ANOS\n");
                    imprimeMatriz(sessao2);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao2.length; i++) {
                            for (int j = 0; j < sessao2.length; j++) {
                                while (sessao2[i][j].equals(pol + inteira) || sessao2[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao2.length; i++) {
                            for (int j = 0; j < sessao2.length; j++) {

                                if (pol.equals(sessao2[i][j].substring(0, sessao2[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao2[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao2[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao2);

                    break;
                case 3:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[3] SALA C | TURMA DA MÔNICA - LAÇOS | 118 MIN | DUB | 14 ANOS\n");
                    imprimeMatriz(sessao3);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao3.length; i++) {
                            for (int j = 0; j < sessao3.length; j++) {
                                while (sessao3[i][j].equals(pol + inteira) || sessao3[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao3.length; i++) {
                            for (int j = 0; j < sessao3.length; j++) {

                                if (pol.equals(sessao3[i][j].substring(0, sessao3[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao3[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao3[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao3);

                    break;
                case 4:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[4] SALA A | HEBE - A ESTRELA DO BRASIL | 122 MIN | DUB | 14 ANOS\n");
                    imprimeMatriz(sessao4);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao4.length; i++) {
                            for (int j = 0; j < sessao4.length; j++) {
                                while (sessao4[i][j].equals(pol + inteira) || sessao4[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao4.length; i++) {
                            for (int j = 0; j < sessao4.length; j++) {

                                if (pol.equals(sessao4[i][j].substring(0, sessao4[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao4[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao4[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao4);

                    break;
                case 5:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[5] SALA B | VAI QUE COLA - O COMEÇO | 88 MIN | DUB | 12 ANOS\n");
                    imprimeMatriz(sessao5);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao5.length; i++) {
                            for (int j = 0; j < sessao5.length; j++) {
                                while (sessao5[i][j].equals(pol + inteira) || sessao5[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao5.length; i++) {
                            for (int j = 0; j < sessao5.length; j++) {

                                if (pol.equals(sessao5[i][j].substring(0, sessao5[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao5[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao5[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao5);

                    break;
                case 6:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[6] SALA C | CORINGA | 16 ANOS | 121 MIN | LEG\n");
                    imprimeMatriz(sessao6);
                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao6.length; i++) {
                            for (int j = 0; j < sessao6.length; j++) {
                                while (sessao6[i][j].equals(pol + inteira) || sessao6[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao6.length; i++) {
                            for (int j = 0; j < sessao6.length; j++) {

                                if (pol.equals(sessao6[i][j].substring(0, sessao6[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao6[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao6[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao6);

                    break;
                case 7:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[7] SALA A | MUSSUN, UM FILME DO CACILDIS | 75 MIN | DUB | 10 ANOS\n");
                    imprimeMatriz(sessao7);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao7.length; i++) {
                            for (int j = 0; j < sessao7.length; j++) {
                                while (sessao7[i][j].equals(pol + inteira) || sessao7[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao7.length; i++) {
                            for (int j = 0; j < sessao7.length; j++) {

                                if (pol.equals(sessao7[i][j].substring(0, sessao7[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao7[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao7[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao7);

                    break;
                case 8:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[8] SALA B | OS PARÇAS 2 | 97 MIN | DUB | 12 ANOS\n");
                    imprimeMatriz(sessao8);

                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao8.length; i++) {
                            for (int j = 0; j < sessao8.length; j++) {
                                while (sessao8[i][j].equals(pol + inteira) || sessao8[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao8.length; i++) {
                            for (int j = 0; j < sessao8.length; j++) {

                                if (pol.equals(sessao8[i][j].substring(0, sessao8[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao8[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao8[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao8);

                    break;
                case 9:

                    sc.nextLine();

                    System.out.println("Você escolheu: ");
                    System.out.println("[9] SALA C | BATE CORAÇÃO | 94 MIN | DUB | 12 ANOS\n");
                    imprimeMatriz(sessao9);
                    do {// 0 meia 1 inteira

                        System.out.println("Selecione a poltrona desejada: ");
                        pol = sc.nextLine();
                        for (int i = 0; i < sessao9.length; i++) {
                            for (int j = 0; j < sessao9.length; j++) {
                                while (sessao9[i][j].equals(pol + inteira) || sessao9[i][j].equals(pol + meia) || !pol.equals("A01") && !pol.equals("A02") && !pol.equals("A03") && !pol.equals("A04") && !pol.equals("A05")
                                        && !pol.equals("A06") && !pol.equals("A07") && !pol.equals("A08") && !pol.equals("A09") && !pol.equals("A10")
                                        && !pol.equals("B01")
                                        && !pol.equals("B02")
                                        && !pol.equals("B03") && !pol.equals("B04") && !pol.equals("B05") && !pol.equals("B06") && !pol.equals("B07")
                                        && !pol.equals("B08") && !pol.equals("B09") && !pol.equals("B10") && !pol.equals("C01") && !pol.equals("C02")
                                        && !pol.equals("C03") && !pol.equals("C04") && !pol.equals("C05") && !pol.equals("C06") && !pol.equals("C07")
                                        && !pol.equals("C08") && !pol.equals("C9") && !pol.equals("C10") && !pol.equals("D01") && !pol.equals("D02")
                                        && !pol.equals("D03") && !pol.equals("D04") && !pol.equals("D05")
                                        && !pol.equals("D06") && !pol.equals("D07") && !pol.equals("D08") && !pol.equals("D09") && !pol.equals("D10")
                                        && !pol.equals("E01") && !pol.equals("E02") && !pol.equals("E03") && !pol.equals("E04") && !pol.equals("E05")
                                        && !pol.equals("E06") && !pol.equals("E07") && !pol.equals("E08") && !pol.equals("E09") && !pol.equals("E10")
                                        && !pol.equals("F01") && !pol.equals("F02")
                                        && !pol.equals("F03") && !pol.equals("F04") && !pol.equals("F05") && !pol.equals("F06") && !pol.equals("F07") && !pol.equals("F08")
                                        && !pol.equals("F09") && !pol.equals("F10") && !pol.equals("G01") && !pol.equals("G02") && !pol.equals("G03") && !pol.equals("G04")
                                        && !pol.equals("G05") && !pol.equals("G06")
                                        && !pol.equals("G07") && !pol.equals("G08") && !pol.equals("G09") && !pol.equals("G10") && !pol.equals("H01")
                                        && !pol.equals("H02") && !pol.equals("H03") && !pol.equals("H04") && !pol.equals("H05") && !pol.equals("H06") && !pol.equals("H07") && !pol.equals("H08")
                                        && !pol.equals("H09") && !pol.equals("H10") && !pol.equals("I01") && !pol.equals("I02") && !pol.equals("I03") && !pol.equals("I04")
                                        && !pol.equals("I05") && !pol.equals("I06") && !pol.equals("I07") && !pol.equals("I08") && !pol.equals("I09") && !pol.equals("I10") && !pol.equals("J01")
                                        && !pol.equals("J02") && !pol.equals("J03") && !pol.equals("J04")
                                        && !pol.equals("J05") && !pol.equals("J06") && !pol.equals("J07") && !pol.equals("J08") && !pol.equals("J09") && !pol.equals("J10")) {
                                    i = 0;
                                    j = 0;
                                    System.out.println("cadeira ocupada ou não existente! digite outra:");
                                    System.out.print("> ");
                                    pol = sc.nextLine();

                                }

                            }
                        }

                        System.out.println("Tipo de ingresso:  1/ para inteira 0 / meia");
                        System.out.print(">");
                        ingresso = sc.nextLine();
                        while (!ingresso.equals("1") && !ingresso.equals("0")) {
                            System.out.println("Tipo de ingresso nao existente");
                            ingresso = sc.nextLine();
                        }

                        for (int i = 0; i < sessao9.length; i++) {
                            for (int j = 0; j < sessao9.length; j++) {

                                if (pol.equals(sessao9[i][j].substring(0, sessao9[i][j].length() - 3))) {
                                    if ("1".equals(ingresso)) {
                                        sessao9[i][j] = pol + inteira;
                                    }
                                    if ("0".equals(ingresso)) {
                                        sessao9[i][j] = pol + meia;
                                    }

                                }
                            }
                        }

                        System.out.println("continuar, Digite S/ para sim e N / para não: ");
                        System.out.print("> ");
                        continuar = sc.nextLine();
                        while (!continuar.equals("n") && !continuar.equals("N") && !continuar.equals("S") && !continuar.equals("s")) {
                            System.out.println("Opção digitada inválida, Digite S/ para sim e N / para não.");
                            System.out.print("> ");
                            continuar = sc.nextLine();
                        }
                        // sc.nextLine();
                    } while (!continuar.equals("n") && !continuar.equals("N"));

                    imprimeMatriz(sessao9);

                    break;
                default:
                    break;

            }

            System.out.println("Deseja comprar ingresso de outro filme? ");
            opcao = sc.next().charAt(0);

        } while (opcao != 'n');

    }

//------------------------------------------------------------------------------
//                                 MÉTODOS
//------------------------------------------------------------------------------          
    // cadastro de poltronas
    public static void cadastraPoltronas(String[][] matriz,
            int maxLin, int maxCol, String disponivel) {

        char fileiras = 'A';
        int poltronas = 1;

        for (int lin = 0; lin < maxLin; lin++) {
            for (int col = 0; col < maxCol; col++) {
                if (col == 9) {
                    matriz[lin][col] = String.valueOf(fileiras)
                            + String.valueOf(poltronas)
                            + disponivel;
                } else {
                    matriz[lin][col] = String.valueOf(fileiras)
                            + "0"
                            + String.valueOf(poltronas)
                            + disponivel;
                }
                poltronas++;
            }
            fileiras++;
            poltronas = 1;
        }
    }

    // randomização entre inteira e meia
    public static int sorteiaTipo(int limInfT, int limSupT) {
        Random rd = new Random();
        return rd.nextInt(limSupT - limInfT + 1) + limInfT;
    }

    // randomização de indices poltronas
    public static int sorteiaPoltrona(int limInfP, int limSupP) {
        Random rd = new Random();
        return rd.nextInt(limSupP - limInfP + 1) + limInfP;
    }

    // preenchimento de poltronas por sessão
    public static void preenchimetoAleatorio(String[][] matriz,
            int limInfP, int limSupP,
            String meia, String inteira) {

        for (int cont = 0; cont < 20; cont++) {
            int tipoIngresso = sorteiaTipo(0, 1);
            int i = sorteiaPoltrona(limInfP, limSupP);
            int j = sorteiaPoltrona(limInfP, limSupP);
            if (tipoIngresso == 1) {
                while (matriz[i][j].substring(3).equals(meia)
                        || matriz[i][j].substring(3).equals(inteira)) {
                    i = sorteiaPoltrona(limInfP, limSupP);
                    j = sorteiaPoltrona(limInfP, limSupP);
                }
                matriz[i][j] = matriz[i][j].substring(0,
                        matriz[i][j].length() - 3) + inteira;
            } else {
                while (matriz[i][j].substring(3).equals(meia)
                        || matriz[i][j].substring(3).equals(inteira)) {
                    i = sorteiaPoltrona(limInfP, limSupP);
                    j = sorteiaPoltrona(limInfP, limSupP);
                }
                matriz[i][j] = matriz[i][j].substring(0,
                        matriz[i][j].length() - 3) + meia;
            }
        }
    }

    // Menu de sessões
    public static void menuSessoes() {
        System.out.println(""
                + "__________________________________________________________________\n"
                + "MANHÃ\n"
                + "[1] SALA A | AS PANTERAS | 118 MIN | DUB | 14 ANOS\n"
                + "[2] SALA B | MALEVOLA - DONA DO MAL | 120 MIN | DUB | 10 ANOS\n"
                + "[3] SALA C | TURMA DA MÔNICA - LAÇOS | 118 MIN | DUB | 14 ANOS\n"
                + "__________________________________________________________________\n"
                + "TARDE\n"
                + "[4] SALA A | HEBE - A ESTRELA DO BRASIL | 122 MIN | DUB | 14 ANOS\n"
                + "[5] SALA B | VAI QUE COLA - O COMEÇO | 88 MIN | DUB | 12 ANOS\n"
                + "[6] SALA C | CORINGA | 16 ANOS | 121 MIN | LEG\n"
                + "__________________________________________________________________\n"
                + "NOITE\n"
                + "[7] SALA A | MUSSUN, UM FILME DO CACILDIS | 75 MIN | DUB | 10 ANOS\n"
                + "[8] SALA B | OS PARÇAS 2 | 97 MIN | DUB | 12 ANOS\n"
                + "[9] SALA C | BATE CORAÇÃO | 94 MIN | DUB | 12 ANOS\n"
                + "__________________________________________________________________\n"
        );
    }

    // Impressão de matriz de poltronas por sessão
    public static void imprimeMatriz(String[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

//------------------------------------------------------------------------------
//                                 RASCUNHO
//------------------------------------------------------------------------------  
/*
 LOGICA INICIAL PARA VENDA DE INGRESSOS
 System.out.println("Informe o código da poltrona que deseja reservar:");
 String poltrona = "";
 poltrona = sc.nextLine();
 // reserva de poltrona
 for (int lin = 0; lin < maxLin; lin++) {
 for (int col = 0; col < maxCol; col++) {
 if (sessao1[col][lin].equals(poltrona + disponivel)) {
 if (tipoIngresso == 1) {
 sessao1[col][lin] = poltrona + inteira;
 } else {
 sessao1[col][lin] = poltrona + meia;
 }
 }
 }
 } */

 /*   
 algoritimo para comparar entre legendas
 if (p1[1][1].substring(3).equals("[DIS.]")) {
 System.out.println("OK");
 } */
